TinyXML Library for Arduino
----

Forked from [Arduino's TinyXML fork](https://github.com/adafruit/TinyXML) to
implement fixes for use on an Arduino Uno WiFi REV2 board.
These changes have not been tested on any other architectures.

`pgm_read_word()` is not supported and is not necessary for the target Arduino
board, and so was simply removed where called.

The Arduino TinyXML port is maintained by Adafruit and is not licensed.
Adafruit's repo itself is a fork from Adam Rudd's
[arduino-sketches repo](https://github.com/adamvr/arduino-sketches),
which is not licensed and has not been updated since 2011.

TinyXML itself (both TinyXML-1 and TinyXML-2) are licensed under the zlib
license. Therefore, this repository is also licensed under the zlib license,
as required by TinyXML's license. See LICENSE.txt for more information.
